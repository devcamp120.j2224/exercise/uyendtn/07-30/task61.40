package com.devcamp.task6070.order.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6070.order.api.model.CProduct;

public interface IProductRepository extends JpaRepository<CProduct,Long> {
    CProduct findById(long orderId);
}
